// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Food.h"
#include "Components/InputComponent.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));

}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	CreateSnakeActor();
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	buferTime += DeltaTime;
	if (buferTime >= spawnTime)
	{
		SpawnFood();
		buferTime = 0;
	}
}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);

	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);

}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
}

void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastMoveDirection!=EMovementDirection::DOWN)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::UP;
		}
		else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::UP)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::DOWN;
		}
	}
	//UE_LOG(LogTemp, Warning, TEXT("Move forvard %f"), value);
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::RIGHT)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::LEFT;
		}
		else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::LEFT)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::RIGHT;
		}
	}
	//UE_LOG(LogTemp, Warning, TEXT("Move right %f"), value);
}

void APlayerPawnBase::SpawnFood()
{
	//�������� ��������� ����������
	float spawnX = FMath::FRandRange(minX, maxX);
	float spawnY = FMath::FRandRange(minY, maxY);
	
	//����������� �� �����
	float spawnGridX = FMath::GridSnap(spawnX, gridSize);
	float spawnGridY = FMath::GridSnap(spawnY, gridSize);
	
	UE_LOG(LogTemp, Warning, TEXT("spawn X %f, Y %f"), spawnGridX, spawnGridY);
	//UE_LOG(LogTemp, Warning, TEXT("spawn Y %f"), spawnGridY);
	
	FVector startFoodPoint = FVector(spawnGridX, spawnGridY, spawnZ);
	FRotator startFoodRotation = FRotator(0.0, 0.0, 0.0);
	
	
	GetWorld()->SpawnActor<AFood>(FoodActorClass, startFoodPoint, startFoodRotation);

}

