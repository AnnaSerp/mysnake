// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnBase.generated.h"

class UCameraComponent;
class ASnakeElementBase;
class ASnakeBase;
class AFood;

UCLASS()
class MYSNAKE_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();

	UPROPERTY(BlueprintReadWrite)
		UCameraComponent* PawnCamera;

	UPROPERTY(BlueprintReadWrite)
		ASnakeBase* SnakeActor;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeBase> SnakeActorClass;

	UPROPERTY()
		TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY(BlueprintReadWrite)
		AFood* FoodActor;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood> FoodActorClass;

	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void CreateSnakeActor();

	UFUNCTION()
		void HandlePlayerVerticalInput(float value);

	UFUNCTION()
		void HandlePlayerHorizontalInput(float value);
	
	//������ �������� ��������� ��� ������ ���
	float maxY = 800.f;
	float minY = -800.f;
	float maxX = 400.f;
	float minX = -400.f;
	float spawnZ = 10.f;
	float gridSize = 50.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float spawnTime = 1.0f;
	float buferTime = 0;

	//��������� ������� �������� ��� � ��������� ������
	void SpawnFood();

};
